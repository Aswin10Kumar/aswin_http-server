const fs = require('fs');
const http = require('http');

const { v4: uuidv4 } = require('uuid');

const port = 8080;

const httpServer = http.createServer(function (req, res) {

    if (req.method == 'GET') {
        const last = req.url.charAt(req.url.length - 1);
        let url = req.url;
        if (last == '/') {
            url = req.url.slice(0, -1);
        }

        if (url == '/html') {

            fs.readFile('index.html', function (err, data) {
                if (err) {
                    res.writeHead(404);
                    res.write('ERROR:File not found');
                } else {
                    res.writeHead(200, { 'Content-Type': 'text/html' })
                    res.write(data);
                }
                res.end();
            })

        }
        else if (url == '/json') {

            fs.readFile('index.json', function (err, data) {
                if (err) {
                    res.writeHead(404);
                    res.write('ERROR:File not found');  
                } else {
                    res.writeHead(200, { 'Content-Type': ' application/json' })
                    res.write(data);
                }
                res.end();
            })

        }
        else if (url == '/uuid') {

            const uuid = uuidv4();
            res.writeHead(200, { 'Content-Type': ' application/json' });
            res.write(uuid);
            res.end();

        }
        else if (/*url.includes('/status')*/url.split('/')[1] == 'status') {

            const urlArray = url.split('/');
            const statusCode = urlArray[2];
            const statusMessage = http.STATUS_CODES[statusCode];

            res.setHeader('Content-Type', 'text/data')
            if (statusMessage && urlArray.length - 1 == 2) {
                res.statusCode = 200;
                res.write(`${statusCode}: ${statusMessage}`);
            }
            else {
                const errMsg = urlArray.length - 1 == 2 ? 'UnKnown status code' : 'Wrong URL';
                if (errMsg === 'Wrong URL') {
                    res.statusCode = 404;
                } else {
                    res.statusCode = 422;
                }
                res.write(errMsg)
            }

            res.end();

        }
        else if (url.split('/')[1] == 'delay') {

            const urlToArray = url.split('/');

            const urlLength = urlToArray.length - 1;
            const timer = +urlToArray[2];

            res.setHeader('Content-Type', 'text/data')
            if (urlLength != 2 || isNaN(timer) || timer == 0) {

                const errMsg = urlLength != 2 ? 'Wrong URL' : 'Delay is not a number';
                if (errMsg === 'Wrong URL') {
                    res.statusCode = 404
                } else {
                    res.statusCode = 422;
                }
                res.write(errMsg);
                res.end();

            }
            else {
                setTimeout(() => {
                    res.statusCode = 200;
                    res.write("SUCCESS");
                    res.end();
                }, timer * 1000)
            }

        }
        else {

            res.writeHead(404, { 'Content-Type': 'text/html' })
            res.write("No such URL found");
            res.end();

        }
    }
    else {
        res.writeHead(405, { 'Content-Type': ' application/json' })
        res.end("Request method is not \"GET\" ");
    }
})

httpServer.listen(port, function () {
    console.log("Server is listening port", port)
})


// setTimeout(() => {
//     httpServer.close((err) => {
//         console.log("stop listening")
//     })
// }, 20000)
